# ss_canon

Matlab/Octave routine to create controllable, observable and diagonal canonical state space matrices from already existing LTI state spaces. 

# How to use

Ensure your state space is in the `sys` variable and run the script. 
Output Controllable Canonical State Space will be in `ss_ctrb` variable and Observable Canonical State Space in the `ss_obsv`.

Example:

With `canon_ss.m` in your current folder, just add:
```
   sys = ss(A,B,C,D);
   run('canon_ss.m');
```
To your script.

# Known issues

Still working on diagonal/Jordan forms.

Transfer functions `minreal` has arbitrary precision that may be too high in some cases.

# Acknowledgments

My greatest thanks to José Irineu Oliveira Silva for the time and insights about the MIMO case.
