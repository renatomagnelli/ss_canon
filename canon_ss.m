%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    canon_ss: Matlab script for generating Canonical State Spaces
%    Copyright (C) 2018 Renato Magnelli <renatomagnelli@gmail.com>

%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.

%    You should have received a copy of the GNU General Public License
%    along with this program.  If not, see <https://www.gnu.org/licenses/>.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ------------------------------------------------------------------------
% Changelog:
% ------------------------------------------------------------------------

% v1.0

% Initial release

% ------------------------------------------------------------------------
% To Do:
% ------------------------------------------------------------------------

% Still not working in Quadrotor 12 states, 4 inputs, 4 outpus observable

% ------------------------------------------------------------------------
% References:
% ------------------------------------------------------------------------

% OGATA, K. Modern Control Engineering. 4 ed. Prentice Hall: Upper Saddle
% River, New Jersey, 2002.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ------------------------------------------------------------------------
% State Space data
% ------------------------------------------------------------------------
   [Am, Bm, Cm, Dm] = ssdata(sys);
    CCF_nstates  = size(Am,1);
    CCF_noutputs = size(Cm,1);
    CCF_ninputs  = size(Bm,2);
    CCF_block_length = CCF_noutputs*CCF_ninputs;

% ------------------------------------------------------------------------
% Transforming to TF and mounting auxiliary matrices
% ------------------------------------------------------------------------
    a = zeros(CCF_block_length, CCF_nstates+1);
    b = zeros(CCF_block_length, CCF_nstates+1);
    for iu = 1:CCF_ninputs
        [num,den] = ss2tf(Am,Bm,Cm,Dm,iu);
        for iy = 1:CCF_noutputs
            G(iy + (iu - 1)*CCF_noutputs) ...
                    = minreal(tf(num(iy,:),den),1e-10);
            [tb,ta] = tfdata(G(iy + (iu - 1)*CCF_noutputs));
            ta = ta{:}; tb = tb{:};
            for t = 1:length(ta)
                a((iu - 1)*CCF_noutputs + iy, t) = ta(length(ta) - t + 1);
            end
            for t = 1:length(tb)
                b((iu - 1)*CCF_noutputs + iy, t) = tb(length(tb) - t + 1);
            end
        end
    end
    % Trim auxiliary parameters matrices
    a(:,end) = [];
    anullcolumns = any(a);
    while anullcolumns(end) == 0
        a(:,end) = [];
        anullcolumns = any(a);
    end
    b0 = b(:,end);
    b(:,(size(a,2) + 1):end) = [];
    c  = b - a.*repmat(b0,1,size(a,2));
    CCF_nstates  = size(a,2);
    CCF_baseline = CCF_block_length*(CCF_nstates-1);

% ------------------------------------------------------------------------
% Canonical State Space Template
% ------------------------------------------------------------------------
    % A
    Actrb = [zeros(CCF_baseline, CCF_block_length)     ...
               eye(CCF_baseline, CCF_baseline    );    ...
             zeros(CCF_block_length, CCF_block_length*CCF_nstates)];
    % B
    Bctrb =  zeros(CCF_baseline,                       ...
                   CCF_ninputs);
    Bobsv =  zeros(CCF_block_length*(CCF_nstates-1),   ...
                   CCF_ninputs);
    for iu = 1:CCF_ninputs
        Bctrb = [Bctrb;                                ...
                 zeros(CCF_noutputs, iu - 1)           ...
                  ones(CCF_noutputs, 1    )            ...
                 zeros(CCF_noutputs, (size(Bm,2) - iu))];
    end
    % C
    Cctrb =  zeros(CCF_noutputs, CCF_block_length*CCF_nstates);
    % D
    Dctrb =  zeros(CCF_noutputs, CCF_ninputs);

% ------------------------------------------------------------------------
% Substituting values into Controllable Canonical State Space Template
% ------------------------------------------------------------------------
    % A
    for ix = 1:CCF_nstates
        Actrb((CCF_baseline + 1):end,      ...
             (1+((ix-1)*CCF_block_length)) ...
             :(ix*CCF_block_length)) = diag(a(:,ix).*(-1));
    end
    % C
    count = 1;
    for ix = 1:CCF_nstates
        for iu = 1:CCF_ninputs
            for iy = 1:CCF_noutputs
                Cctrb(iy, count) ...
                   = c(iy + CCF_noutputs*(iu-1),ix);
                count = count + 1;
            end
        end
    end
    % D
    count = 1;
    for iu = 1:CCF_ninputs
        for iy = 1:CCF_noutputs
            Dctrb(iy, iu) = b0(count);
            count = count + 1;
        end
    end

% ------------------------------------------------------------------------
% Substituting values into Observable Canonical State Space
% ------------------------------------------------------------------------
    % A
    Aobsv = Actrb';
    % B
    count = 1;
    for ix = 1:CCF_nstates
        for iu = 1:CCF_ninputs
            for iy = 1:CCF_noutputs
                Bobsv(count, iu) ...
                   = c(iy + CCF_noutputs*(iu-1),ix);
                count = count + 1;
            end
        end
    end
    % C
    Cobsv   = [zeros(CCF_noutputs, CCF_baseline) ...
               repmat(eye(CCF_noutputs, CCF_noutputs), 1, CCF_ninputs)];
    % D
    Dobsv   = Dctrb;

% ------------------------------------------------------------------------
% State Spaces in observable and controllable MIMO canonical forms
% ------------------------------------------------------------------------

    ss_ctrb = ss(Actrb, Bctrb, Cctrb, Dctrb);
    ss_obsv = ss(Aobsv, Bobsv, Cobsv, Dobsv);
